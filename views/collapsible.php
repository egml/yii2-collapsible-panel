<?php
	// ID виджета
	$id = $this->context->id;

	// initiallyCollapsed
	$if = $this->context->initiallyCollapsed;
?>
<div class="panel-group" id="<?= $id ?>" role="tablist" aria-multiselectable="true">
	<div class="panel panel-default">
		<div class="panel-heading" role="tab" id="heading_<?= $id ?>">
			<span class="admin-panel-heading-icon glyphicon glyphicon-triangle-<?= $if ? 'right' : 'bottom' ?>"></span>
			<a role="button" data-toggle="collapse" data-parent="#<?= $id ?>" href="#collapse_<?= $id ?>" aria-expanded="<?= $if ? 'false' : 'true' ?>" aria-controls="collapse_<?= $id ?>">
				<?= $this->context->heading ?>
			</a>
		</div>
		<div id="collapse_<?= $id ?>" class="panel-collapse collapse<?= $if ? '' : ' in' ?>" role="tabpanel" aria-labelledby="heading_<?= $id ?>">
			<div class="panel-body">
				<?= $body ?>
			</div>
		</div>
	</div>
</div>
