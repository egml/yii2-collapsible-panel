<div class="panel panel-default">
	<?php if ($this->context->displayHeading): ?>
		<div class="panel-heading"><?= $this->context->heading ?></div>
	<?php endif ?>
	<div class="panel-body">
		<?= $body ?>
	</div>
</div>
