<?php
namespace egml\collapsiblePanel;

class Widget extends \yii\base\Widget
{
	public $heading = 'Заголовок';
	public $displayHeading = true;
	public $collapsible = false;
	public $initiallyCollapsed = false;

    public function init()
	{
		$this->view->registerAssetBundle('yii\\bootstrap\\BootstrapAsset');

		if ($this->collapsible) {
			$script = <<<SCRIPT
// Виджет сворачивающихся панелей
$('.panel-collapse').on('show.bs.collapse', function() {
	$(this).siblings('.panel-heading').find('.admin-panel-heading-icon').removeClass('glyphicon-triangle-right').addClass('glyphicon-triangle-bottom');
}).on('hide.bs.collapse', function() {
	$(this).siblings('.panel-heading').find('.admin-panel-heading-icon').removeClass('glyphicon-triangle-bottom').addClass('glyphicon-triangle-right');
});
SCRIPT;
			$this->view->registerJs($script);
			$this->view->registerAssetBundle('yii\\bootstrap\\BootstrapPluginAsset');
		}
		ob_start();
	}

	public function run()
	{
		$body = ob_get_clean();
		if ($this->collapsible) {
			$view = 'collapsible';
		} else {
			$view = 'static';
		}
		return $this->render($view, [
			'body' => $body,
		]);
	}
}
