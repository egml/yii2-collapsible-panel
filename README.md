## Сворачиваемые области для Yii2

На базе компонента [Panel](https://getbootstrap.com/docs/3.3/components/#panels) из Twitter Bootstrap 3, только добавлен функционал опционального сворачивания целой области и контроля над стартовым состоянием (свернуто/развернуто). Может применяться для различных целей, например, группировка полей формы.

Пример использования:

	<?php
		CollapsiblePanel::begin([
			// Значения по умолчанию:
			// 'heading' => 'Заголовок',
			// 'displayHeading' => true,
			// 'collapsible' => false,
			// 'initiallyCollapsed' => false,
		]);
	?>

	Lorem ipsum dolor sit amet consectetur adipisicing elit. Enim in 
	odio eius libero deleniti. Ut aliquam deleniti quas enim quia, suscipit 
	deserunt delectus architecto numquam natus, qui ab itaque veniam.
	
	<?php CollapsiblePanel::end() ?>
